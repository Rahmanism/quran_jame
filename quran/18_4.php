<?php require_once('ttop.inc'); ?>
<p>محمّد بن اسماعيل گفت: به اتفاق عمويم عبد الرحمن حضور حضرت صادق -ع- شرف ياب شدم. از من پرسيدند:‌ فرزند اسماعيل هستي؟ عرض كردم: بلي. حضرت،‌ طلب مغفرت نمود. سپس به ما فرمود:<br />
مودّت و محبّت ما ائمه را كوچك نشماريد، زيرا مودت ما، از جمله باقيات صالحات است.<br />
عرض كرديم: اي فرزند رسول خدا! سپاس گزاريم كه خداوند، نعمت دوستي شما اهل بيت را به ما عطا نموده. <br />
فرمود: بگوئيد: «الحمدلله علي اول النعم» <br />
عرض كرديم: اول نعمت كدام است؟<br />
فرمود: ولايت ما آل محمّد -ص- است. </p> 
<p>تأويل الآيات،‌ ج 1، ص 297،‌ ح 8.</p>
<?php require_once('tbot.inc'); ?>