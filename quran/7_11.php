<?php require_once('ttop.inc'); ?>
<p>حضرت صادق -ع- فرمود: الواح مزبور جفر است، و حاوي علوم اولين و آخرين است، و آن جفر نزد ماست. علاوه بر آنها عصاي موسي -ع- هم نزد ما مي‌باشد، و ما وارث پيغمبران سلف هستيم.</p>
<p>تفسير عياشي، ج 2، ص 28، ح 77.</p>
<?php require_once('tbot.inc'); ?>