<?php require_once('ttop.inc'); ?>
<p>در كافي، از ابو بصير روايت كرده كه امام صادق -ع- فرمود: آنگاه حضرت داود، علم پيغمبران پيش از خود را ارث برد. و سليمان، وارث علم داود شد، و محمّد -ص- وارث علم سليمان گرديد. و ما ائمه وارث محمّد -ص- هستيم. و صحف ابراهيم و الواح موسي -ع- نزد ما است.   </p> 
<p>كافي، ج 1، ص 314، ح 3.</p>
<?php require_once('tbot.inc'); ?>