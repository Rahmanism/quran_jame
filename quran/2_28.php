<?php require_once('ttop.inc'); ?>
امام حسن عسكري -ع- فرمود: امام باقر -ع- در مورد آيه «أو كلما عاهدوا» فرمود: خداوند مي فرمايد: يهوديان و ناصبياني كه ذكرشان به عمل آمد، بر عهدي كه بر ايمان به محمد -ص- و علي -ع- بسته بودند، پايدار نماندندونقض عهد كردندو پيمان شكني و مخالفت امر خدا نمودند. و با آن همه معجزه كه از پيغمبر مشاهده نمودند، باز بر عناد خود باقي ماندندو نا فرماني كردند. بيشتر آنها،‌در آينده هم توبه نكنند و ايمان نياورند.
<p>تفسير امام حسن عسكري -ع-، ص 464، ح302. 

<?php require_once('tbot.inc'); ?>