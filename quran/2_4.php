﻿<?php require_once('ttop.inc'); ?>
حضرت موسي بن جعفر -ع- فرمود: منافقين، بر اثر معجزات مشهوده آن حضرت علاوه بر 
آن كه دلهاي مريض داشتند، بر بيماري دل آنها بيفزود. و به واسطه نقض عهد خداوند و 
شكستن بيعت با اميرالمؤمنين -ع- مرضي بر امراض قبلي اينان زياد شد، و درباره علي 
-ع- به حيرت افتادند. و به مناسبت تكذيب امر خداوند - كه به دروغ مدعي بودند كه، بر 
بيعت خود استوار و پايدار هستيم - به كيفر شديد و عذابي سخت، دچار خواهند شد.
<p>تفسير امام عسكري -ع، ص 117.
<?php require_once('tbot.inc'); ?>