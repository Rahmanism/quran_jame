<?php require_once('ttop.inc'); ?>
<p>محمد بن عباس از ابي محمد حنّاط روايت كرده كه گفت: از حضرت باقر -ع- معناي آيه «و انه لفي زبر الاولين» را پرسيدم. فرمود: مقصود، ولايت اميرالمؤمنين -ع- است كه در كتب پيغمبران سلف هم بيان شده است.    </p> 
<p>تأويل الآيات، ج 1، ص 391، ح 16.</p>
<?php require_once('tbot.inc'); ?>