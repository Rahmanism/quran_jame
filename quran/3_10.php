<?php require_once('ttop.inc'); ?>
عياشي از عمار بن مروان روايت كرده كه گفت: از حضرت صادق -ع- در مورد آيه « افمن اتبع رضوان الله كمن باء بسخط من الله-تا آخر آيه » سؤال نمودم. فرمود: مراد از كساني كه پيروي از رضايت و خوشنودي خداوند مي‌نمايند، ائمه هستند. و به سبب موالات و معرفت در حق ائمه -ع- ، و حسنات مؤمنين زياد شده، و خداوند درجات آنها را بلند مي‌كند.
و مراد از « كمن باء بسخط » به خدا قسم آنها هستند كه منكر ولايت اميرالمؤمنين و ائمه طاهرين -ع- مي‌باشند.و از اين جهت قدم بر نمي‌دارند، مگر به سوي غضب خداوند.
<P>منبع: تفسير عياشي،ج 1،ص 205،ح 149.
<?php require_once('tbot.inc'); ?>
