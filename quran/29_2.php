<?php require_once('ttop.inc'); ?>
<p>محمد بن عباس، روايت كرده كه آيه فوق در حق اميرالمؤمنين -ع- و حمزه و عبيده نازل شد. و آيه «ام حسب الذين يعملون السيئات» درباره عتبه و شيبه و وليدبن عتبه فرود آمده، زيرا اينان با آن بزرگواران، در جنگ، مبارزه و قتال نموند.</p> 
<p>تأويل الآيات، ج 1، ص 429، ح 6؛ و به طريق عامّه: شواهد التنزيل، ج 2، ص 440، ح 604.</p>
<?php require_once('tbot.inc'); ?>