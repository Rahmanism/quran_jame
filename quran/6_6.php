<?php require_once('ttop.inc'); ?>
<p>عياشي از حضرت باقر -ع- روايت كرده كه فرمود: روزي قنبر، غلام اميرالمؤمنين -ع- بر حجّاج وارد شد. از او سؤال كرد: اي قنبر! چه كارهايي براي اميرالمؤمنين -ع- انجام مي‌دادي؟ گفت: آب وضوي آن حضرت را حاضر مي‌كردم. پرسش كرد: پس از فراغت از نماز، چه آيه‌اي تلاوت مي‌فرمودند؟ عرض كرد: اين آيه را - «فَلَمَّا نَسُواْ مَا ذُكِّرُواْ...» - تا آخر «وَالْحَمْدُ لِلّهِ رَبِّ الْعَالَمِينَ».<br />
حجّاج گفت: آيا اين آيات را به ما بني‌اميه تأويل مي‌كرد؟ پاسخ داد: آري. حجّاج گفت: اگر تو را به قتل برسانم، چه كاري انجام مي‌دهي؟ عرض كرد: در اين صورت، شخص سعيدي را فردي شقي به قتل رسانيده است.<br />
حجّاج امر كرد، گردن قنبر را زدند.</p>
<p>تفسير عياشي، ج 1، ص 359، ح 22.</p>
<?php require_once('tbot.inc'); ?>