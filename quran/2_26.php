<?php require_once('ttop.inc'); ?>
حضرت عسكري -ع- در تفسير آيه «و لقد جائكم موسي بالبينات» فرمود: خداوند به 
يهودي‌هاي سابق الذكر مي‌فرمايد: موسي براي شما آيات و دلايلي بر شناسايي نبوت محمد 
-ص- و فضيلت او بيان نمود و ولايت علي و اولادش -ع- را بر شما ثابت نمود و 
شناسانيد. پس از آن كه هارون را به جاي خود گذاشت و به ميقات رفت، شما گوساله‌پرست 
شديد، و با اين عمل به نفس خود ستم كرديد و كافر شديد.
<p>تفسير امام حسن عسكري-ع، ص 408، ح 278.
<?php require_once('tbot.inc'); ?>