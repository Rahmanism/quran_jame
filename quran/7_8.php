<?php require_once('ttop.inc'); ?>
<p>عياشي از ابي‌ذر غفاري روايت كرده كه گفت: به خدا قسم پيغمبر -ص- فرمود: هيچ كس به عهدي كه خداوند در عالم ذر از او گرفته تصديق نكرده و وفاي به آن عهد ننموده، جز اهل بيت پيغمبر اكرم -ص، و اندكي از شيعيان آنها. و اين آيه را تلاوت نمود.</p>
<p>تفسير عياشي، ج 2، ص 23، ح 59.</p>
<?php require_once('tbot.inc'); ?>