<?php require_once('ttop.inc'); ?>
<p>ابن شهر آشوب، از ابن عباس ذيل اين آيه روايت كرده كه گفت:<br />
به خدا قسم «صراط» محمّد -ص- و اهل بيت او هستند. و مراد از «من اهتدي» اصحاب با وفاي پيغمبر اكرم -ص- مي‌باشند كه مسلمين را به ولايت اميرالمؤمنين علي بن ابيطالب -ع- هدايت مي‌كنند.</p> 
<p>برهان، ج 3، ص 51، ح 12.</p>
<?php require_once('tbot.inc'); ?>