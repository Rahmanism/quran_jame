<?php require_once('ttop.inc'); ?>
<p>عياشي به سند خود از عبدالله بن سليمان روايت كرده كه گفت: از حضرت صادق -ع- معناي آيه: «قد جاءكم بُرْهانٌ مِنْ ربِّكُمْ و انْزَلْنا اليكم نوراً مُبيناً» را پرسيدم: فرمود: «برهان» وجود مقدس پيغمبر اكرم -ص- است. و «نور»، اميرالمؤمنين -ع- مي‌باشد. عرض كردم: «صراط مستقيم» يعني چه؟ فرمود: وجود مقدس اميرالمؤمنين -ع- است. گفتم: «فأمّا الذين آمنوا بالله و اعتصموا به» يعني چه؟ فرمود: مراد، كساني هستند كه به ولايت اميرالمؤمنين و ائمه طاهرين -ع- تمسك جسته و چنگ زدند.</p>
<p>منبع: تفسير عياشي: ج 1، ص 285، ح 308. </p>
<?php require_once('tbot.inc'); ?>
  