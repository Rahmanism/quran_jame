﻿<?php require_once('ttop.inc'); ?>
عياشي به سند خود از فضيل بن يسار از حضرت باقر -ع-، و به سند ديگر از ابي بصير از حضرت صادق -ع- روايت كردند كه فرمودند: به خدا قسم ما ائمه -ع- راسخون در علم هستيم، و ما تأويلات متشابهات قرآن را مي دانيم.
<P>منبع:1- (تفسير كافي، ج 1،ص 213،ح 1)<br>منبع: 2-  (تفسير عياشي، ج 1،ص 164،ح 7و8).
<P>و نيز به سند خود از حضرت صادق -ع- روايت كرده كه مراد از امّ‌الكتاب، اميرالمؤمنين و ائمه طاهرين -ع- و منظور از متشابهات، منافقين، و غرض از « فامّا الذين في قلوبهم زيغ » اصحاب و دوستان منافقين مي باشد.
<P>منبع:تفسير كافي، ج 1،ص 414،ح 14.
<?php require_once('tbot.inc'); ?>