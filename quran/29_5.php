<?php require_once('ttop.inc'); ?>
<p>علي بن ابراهيم قمي از محمد بن مسلم روايت كرده كه امام محمد باقر -ع- فرمود: امام سجاد -ع- فرموده است: هر فرد مؤمن كه ديدگانش در قتل امام حسين -ع- گريان شود تا آنجا كه اشك بر گونه‌هايش سرازير گردد، خداوند، او را در بهشت، در غرفه‌هايي ساكن گرداند كه سالهاي دراز در آن سكونت كند.</p> 
<p>تفسير قمي، ج 2، ص 291.</p>
<?php require_once('tbot.inc'); ?>