<?php require_once('ttop.inc'); ?>
<p>ابن شهر آشوب در معني آيه از اميرالمؤمنين -ع- روايت كرده كه آن حضرت فرمود: وسيله، من و فرزندانم مي‌باشيم. هر گاه شخصي از خداوند چيزي درخواست كند، ما را واسطه و وسيله درخواست خود در درگاه ربوبي قرار دهد، تا خداوند حاجات او را بر آورد و انجام دهد.<br />
و اين حديث را صفار در بصائر الدرجات به سند خود از سلمان فارسي روايت كرده است.</p>
<p>برهان، ج 1، ص 469، ح 2 و 3.</p>
<?php require_once('tbot.inc'); ?>