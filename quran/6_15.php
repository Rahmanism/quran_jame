<?php require_once('ttop.inc'); ?>
<p>محمد بن حسن صفار در كتاب بصائر الدرجات به سند خود از بي حمزه ثمالي روايت كرده كه گفت: از حضرت صادق -ع- معناي آيه: «وَأَنَّ هَذَا صِرَاطِي مُسْتَقِيمًا» را سؤال نمودم، فرمود: به خدا قسم صراط و ميزان، علي بن ابيطالب -ع- است.</p>
<p>بصائر الدرجات، ص 99، ح 9.</p>
<p>و در روايت ديگر فرمود: ما آن راه مستقيم هستيم. و هر كس از اين راه تجاوز كند، كافر مي‌شود.</p>
<p>برهان، ج 1، ص 563، ح 1.</p>
<?php require_once('tbot.inc'); ?>