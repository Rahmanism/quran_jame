<?php require_once('ttop.inc'); ?>
<p>در كافي ذيل آيه «و اذ قالوا اللهم» از حضرت صادق -ع- روايت كرده كه فرمود: روزي پيغمبر اكرم -ص- با جمعي از اصحاب نشسته بودند، كه اميرالمؤمنين -ع- شرفياب حضور مبارك شدند. پيغمبر -ص- به آن حضرت فرمود: تو چون عيسي بن مريم هستي. اگر بيم آن نبود كه عده‌اي درباره‌ات غلو كنند، همان طوري كه نصاري درباره عيسي مي‌گويند، حقايقي در مرتبت و مقامت مي‌گفتم كه مردم رهگذر، خاك پايت را به قصد تبرك و تيمن بردارند.</p>
<p>كافي، ج 8، ص 57، ح 18.</p>
<?php require_once('tbot.inc'); ?>