<?php require_once('ttop.inc'); ?>
<p>عياشي ذيل آيه «و بشّر الذين آمنوا» از حضرت صادق -ع- روايت كرده كه فرمود: مراد از ايمان در آيه، ولايت اميرالؤمنين و ائمه معصومين  -ع-  است. و منظور از قدم صدق، رسول اكرم و شفاعت او واهل بيت  -ع-  باشد.</p> 
<p>مجمع البيان، ج3، ص8. <br />
تفسير برهان، ج2، ص177، ح1-7.</p>
<?php require_once('tbot.inc'); ?>