<?php require_once('ttop.inc'); ?>
<p>ابن بابويه از حضرت صادق -ع- در آيه: «يَوْمَ يَأْتِي بَعْضُ آيَاتِ رَبِّكَ لاَ يَنفَعُ نَفْسًا إِيمَانُهَا» روايت كرده كه فرمود: مراد از «آيات» ائمه هستند. و آيه منتظره، حضرت حجت امام زمان -ع- مي‌باشد. و كسي كه پيش از قيام آن حضرت ايمان نياورده باشد، ايمانش سودي ندهد.</p>
<p>كمال الدين و تمام النعمة، ص 336، ح 8.</p>
<?php require_once('tbot.inc'); ?>