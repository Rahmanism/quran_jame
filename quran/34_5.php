<?php require_once('ttop.inc'); ?>
<p>در كافي، از ابو حمزه روايت كرده كه گفت: از حضرت باقر -ع- معناي آيه «قل انما اعظكم بواحدة» را پرسيدم. فرمود: مراد از آن يك پند و نصيحت كه خداوند مي‌فرمايد، همان دوستي و ولايت اميرالمؤمنين -ع- است.</p>
<p>كافي، ج 1، ص 347، ح 41.</p>
<?php require_once('tbot.inc'); ?>