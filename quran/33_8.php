<?php require_once('ttop.inc'); ?>
<p>در كتاب امالي از حضرت باقر و او ار پدرش و او از جدش اميرالمؤمنين -ع- روايت كرده كه فرمود: رسول اكرم -ص- فرمود: هر كس بر من صلوات بفرستد و بر آل من صلوات نفرستد، بوي بهشت را ـ با آن كه از پانصد سال راه به مشام مي‌رسد ـ استشمام نخواهد كرد.<br />
<p> امالي الصدوق،‌ ص 167، ح 9.</p>
اين حديث را عامّه نيز به طريق عديده نقل نموده‌اند.</p>
<p> رجوع شود به كتاب: شرح و فضائل صلوات، نوشته سيد احمد اردكاني.</p>
<?php require_once('tbot.inc'); ?>