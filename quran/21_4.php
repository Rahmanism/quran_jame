<?php require_once('ttop.inc'); ?>
<p>از حضرت زين العابدين و حضرت اميرالمؤمنين علي -ع- روايت كرده كه فرموده‌اند:<br />
مراد از «ميزان»، پيغمبران و اوصياء آنها مي‌باشند.<br />
<p> كافي، ج 1، ص 419، ح 36.</p>
و نيز يكي از القاب و حضرت اميرالمؤمنين علي -ع- «ميزان الاعمال» است.</p> 
<p>بحارالانوار، ج 100، ص 330، ح 29.</p>
<?php require_once('tbot.inc'); ?>