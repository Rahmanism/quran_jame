<?php require_once('ttop.inc'); ?>
 ابن بابويه به سند خود از حضرت صادق -ع- روايت كرده كه در تفسير همين آيه فرمود: مراد از كلمات همان چيزي است كه خداوند به آدم تعليم فرمود كه با خواندنش توبه او قبول شد، و آن عبارت است از:
 <P>«اللهم اني اسئلك بحق محمد و علي و فاطمه و الحسن و الحسين الّا تبّت عليّ».
<P> پس توبه او قبول شد، و خداوند توبه پذير و مهربان است.
 <P>منبع: مجمع البيان، ج 1، ص 200.


<?php require_once('tbot.inc'); ?>