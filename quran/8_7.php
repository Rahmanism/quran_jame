<?php require_once('ttop.inc'); ?>
<p>عياشي از جابر روايت كرده كه گفت: معني آيه را از حضرت باقر -ع- سؤال كردم، فرمود: اين آيه درباره بني‌اميه نازل شده، زيرا آنها بدترين مخلوقات مي‌باشند. و به قرآن كافر شدند و هرگز به پروردگار و روز قيامت ايمان نياورده و نخواهند آورد.</p>
<p>تفسير عياشي، ج 2، ص 65، ح 72؛ تفسير قمي، ج 1، ص 279.</p>
<?php require_once('tbot.inc'); ?>