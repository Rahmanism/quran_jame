<?php require_once('ttop.inc'); ?>
<p>ابن شهر آشوب ذيل اين آيه، از ابن مسعود روايت كرده كه گفت: پيغمبر اكرم -ص- فرمود: اين آيه در شأن علي بن ابيطالب و حضرت فاطمه و حسنين -ع- نازل شده، زيرا آنها بر بلاها و طاعت پروردگار در دنيا صبر نمودند. و در عالم آخرت رستگار مي‌باشند. </p> 
<p>برهان، ج 3، ص 122، ح 1.</p>
<?php require_once('tbot.inc'); ?>