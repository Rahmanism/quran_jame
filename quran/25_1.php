<?php require_once('ttop.inc'); ?>
<p>از حضرت باقر -ع- روايت كرده كه مقصود از «سبيل» در آيه «فلا يستطيعون سبيلا»، وجود مقدس اميرالمؤمنين -ع- است. يعني ظالمين راهي به سوي ولايت اميرالمؤمنين -ع- پيدا نمي‌كنند.  </p> 
<p>تفسير قمي، ج 2، ص 111 - 112.</p>
<?php require_once('tbot.inc'); ?>