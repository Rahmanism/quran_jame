<?php require_once('ttop.inc'); ?>
<p>در كافي از حضرت صادق -ع- روايت كرده كه فرمود: <br />
مراد آن است: كسي كه از ولايت اميرالمؤمنين -ع- اعراض نموده و دوري بجويد.<br />
و فرمود: چنين شخصي در قيامت كور مي‌شود، و به جهت آن كه در دنيا كور باطن بوده، و دلش از ولايت اميرالمؤمنين -ع-  نابيناست. و خداوند مي‌فرمايد: من او را رها كنم، و در آتش جهنم خواهم افكند، براي خاطر آن كه در دنيا، ائمه دين را واگذاشته‌اند، و اوامر آنها را اطاعت نكرده‌اند، و به فرموده‌هاي ايشان گوش نداده‌اند. و چون به آيات خداوند - كه اميرالمؤمنين و ائمه معصومين -ع- هستند - ايمان نياوردند و به  ولايت به ايشان توسّل نجستند، ائمه -ع- نيز در قيامت او را رها مي‌كنند، و به عذاب سخت و دردناكي گرفتار مي‌شود.</p> 
<p>كافي، ج 1، ص 435، ح 92.</p>
<?php require_once('tbot.inc'); ?>