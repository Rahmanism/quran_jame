<?php require_once('ttop.inc'); ?>
<p>محمد بن عباس، از حضرت باقر و حضرت صادق -ع- روايت كرده كه فرمودند: آيه فوق در حقّ اميرالمؤمنين -ع- و حمزه و جعفر و عبيدة بن حارث نازل شد.<br />
مقصود از «و منهم من قضي نحبه» يكي حمزه است كه در جنگ احد شهيد شد. و ديگري جعفر است كه در جنگ موته به درجه رفيع شهادت فائز گرديد. و مراد از « و منهم من ينتظر» اميرالمؤمنين -ع- است، كه پيوسته در انتظار شهادت بود، تا ابن ملجم مرادي، آن بزرگوار را شهيد كردند.<br />
اين حديث ابن بابويه و ابن شهرآشوب، از آن حضرت و حضرت باقر -ع- روايت كرده‌اند.</p> 
<p>تأويل الآيات، ج 2، ص 449، ح 8.</p>
<?php require_once('tbot.inc'); ?>